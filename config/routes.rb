Rails.application.routes.draw do
 root 'contents#index'
 
 resources :contents
   resources :contents do
  end
  
  resources :comments
  resources :contents do
   member do
    get :comments
    # get '/news', to: 'comments'
   end
  end
  get 'session/new'

 # Navbar
 get '/newcomments' => 'comments#index'
 get '/submit' => 'contents#new'
 get '/about' => 'rad#about'
 get '/signup' => 'users#signup'
 
 # Session
 get    '/login',   to: 'sessions#new'
 post   '/login',   to: 'sessions#create'
 get    '/logout',  to: 'sessions#destroy'
 
 get '/v0/item/c:id' => 'v0#getAPIC', format: true
 get '/v0/item/n:id' => 'v0#getAPIN', format: true
 post '/v0/item/create' => 'v0#postAPI'
 get 'item' => 'v0#items'
 
 resources :users
 
end

