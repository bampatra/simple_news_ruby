class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :comment_title
      t.string  :comment_author
      t.integer :content_id
      t.references :content

      t.timestamps null: false
    end
  end
end
