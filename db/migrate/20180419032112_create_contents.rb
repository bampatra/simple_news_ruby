class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.string :news_title
      t.string :news_website
      t.string :news_source
      t.string :author

      t.timestamps null: false
    end
  end
end
