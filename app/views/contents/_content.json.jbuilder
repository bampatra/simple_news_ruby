json.extract! content, :id, :news_title, :news_source, :author, :comment, :created_at.to_time.to_i,:updated_at
json.url content_url(content, format: :json)

