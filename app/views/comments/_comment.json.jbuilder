json.extract! comment, :id, :comment_title, :content_id, :created_at.to_time.to_i, :updated_at
json.url comment_url(comment, format: :json)
