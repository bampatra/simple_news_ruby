module ContentsHelper
  def current_content
    @current_content ||= Content.find_by(id:[:content_id])
  end
end
