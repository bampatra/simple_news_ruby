class ContentsController < ApplicationController
  before_action :set_content, only: [:show, :edit, :update, :destroy, :next]
   before_action :set_page, only: [:index]
  before_filter :require_login, :only=>[:new, :show, :comments]

  # GET /contents
  # GET /contents.json
  def index
    # The news home page will show eight news per page
    @contents = Content.order('created_at DESC').limit(8).offset(@page.to_i * 8)
  end

  # GET /contents/1
  # GET /contents/1.json
  def show
  end

  # GET /contents/new
  def new
    @content = Content.new
  end

  # GET /contents/1/edit
  def edit
  end

  # POST /contents
  # POST /contents.json
  def create
    @content = Content.new(content_params)

    respond_to do |format|
      if @content.save
        format.html { redirect_to '/', notice: 'Content was successfully created.' }
        format.json { render :show, :created, location: @content , status:200 , message:"OK" }
          
      else
        format.html { render :new }
        format.json { render json: @content.errors, status: :unprocessable_entity , message:"Errors" }
      end
    end
  end

  # PATCH/PUT /contents/1
  # PATCH/PUT /contents/1.json
  def update
    respond_to do |format|
      if @content.update(content_params)
        format.html { redirect_to @content, notice: 'Content was successfully updated.' }
        format.json { render :show, status: :ok, location: @content }
      else
        format.html { render :edit }
        format.json { render json: @content.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contents/1
  # DELETE /contents/1.json
  def destroy
    @content.destroy
    respond_to do |format|
      format.html { redirect_to contents_url, notice: 'Content was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  # GET /contents/1/comments
  def comments
    @content = Content.find(params[:id])
    @comment = @content.comment.order('created_at DESC')
    @comment_in_content = Comment.new
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_content
      @content = Content.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def content_params
      params.require(:content).permit(:news_title, :news_website, :news_source, :author)
    end

  def set_page
    @page = params[:page]
  end
end
