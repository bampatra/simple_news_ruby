class V0Controller < ApplicationController
    before_filter :require_login, :only=>[:items]
    def getAPIN
      if Content.where("id" => params[:id]).present?
        @content = Content.find(params[:id])
          render :json => {
          status: 200,
          message: "OK",
          by: @content.author,
          id: @content.id,
          time: @content.created_at.to_time.to_i,
          title: @content.news_title,
          type: "news",
          url: @content.news_source
        },
        status: :ok
      else
       render :json => {
          status: 400,
          message: "Errors",
        },
        status: :bad_request
      
      end
    end
  
   def getAPIC
   
  if Comment.where("id" => params[:id]).present?
     @comment = Comment.find(params[:id])
     render :json => {
      status: 200,
      message: "OK",
      by: @comment.comment_author,
      id: @comment.id,
      time: @comment.created_at.to_time.to_i,
      title: @comment.comment_title,
      type: "comment"
    },  status: :ok
  else
    render :json => {
      status: 400,
      message: "Errors",
    },  status: :bad_request
  end
   end

    def items
        @content = Content.find(params[:id])
        @comment = @content.comment.order('created_at DESC')
        @comment_in_content = Comment.new
    end
    
  def postAPI
      if (params[:type]== "news")
      item = Content.new(content_params)
      if item.save
        render :json => {status: "success",:link => item },status: :ok
      else
        render :json => {status: "fail",:link => item },status: :bad_request
      end
      else (params[:type] = "comment")
      item = Comment.new(comment_params)
      if item.save
        render :json => {status: "success",:link => item },status: :ok
      else
        render :json => {status: "fail"},status: :bad_request
      end
      end
  end
  
  def content_params
    params.permit(:news_title, :news_source, :author)
  end
  
  def comment_params
    params.permit(:comment_author, :comment_title, :content, :content_id)
  end


end
