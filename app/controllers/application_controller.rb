class ApplicationController < ActionController::Base
  # before_filter :require_login
  # protect_from_forgery with: :exception
  
  include SessionHelper
  
  def application
  end
  
  def require_login
    if current_user.nil?
      redirect_to login_path
    end
  end
end

