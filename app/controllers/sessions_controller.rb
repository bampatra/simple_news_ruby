class SessionsController < ApplicationController
  skip_before_filter :require_login
  def new
    @user = User.new
  end
  
  def create
    user = User.find_by(name: params[:session][:name].downcase)
    if user && user.authenticate(params[:session][:password])
      session[:user_id] = user.id
      log_in user
      redirect_to '/'
      # session[:return_to] ||= request.referer
      flash[:success] = "Logged in!"
    else
      flash[:danger] = 'Invalid name and/or password'
      redirect_to '/login'
    end
  end

  def destroy
  session[:user_id] = nil
  redirect_to '/'
  flash[:success] = "Logged out!"
  end
  
end
