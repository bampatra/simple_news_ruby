class UsersController < ApplicationController
   skip_before_filter :require_login
  def signup
    @user = User.new
  end
  
  def show    
    @user = User.find(params[:id])   
  end
  
  def create
    @user = User.new(user_params) 
    if @user.save       
      flash[:success] = "Sign Up Successful!"
      redirect_to @user     
    else       
      redirect_to '/login'
      flash[:danger] = "Error, please try again"
    end   
  end
  
  def delete
    @user = User.find(params[:id])   
    if @user.destroy       
      flash[:success] = "User have been removed!"
      redirect_to '/login'     
    else       
      render @user     
    end   
  end
  
  private 
  def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end