require 'bcrypt'

class User < ApplicationRecord
  VALID_NAME_REGEX = /\A[a-z0-9\-_]+\z/i
  validates :name, presence: true, length: {in: 2..15}, format: { with: VALID_NAME_REGEX }, uniqueness: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: {maximum: 255}, format: { with: VALID_EMAIL_REGEX }, uniqueness: true
  # before_save {self.email = email.downcase}

# Why not {case_sensitive: false}?
# When the case_sensitive is false, if the database has email “noname@example.com”,
# the user can type Noname@example.com and the system will still consider it as ‘unique’

  VALID_PASSWORD_REGEX = /(?=.{10,}$)((?=.*\d)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*/
  has_secure_password
  validates :password, presence: true, length: {minimum: 10}, format: {with: VALID_PASSWORD_REGEX}

  has_many :content, dependent: :destroy
  has_many :comment, dependent: :destroy
  

end
