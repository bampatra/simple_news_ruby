class Comment < ActiveRecord::Base
    belongs_to :user
    belongs_to :content
 
    validates :comment_title, presence: true, length: { in: 3...1000 }
    # validates_length_of :comment_title, :minimum => 3
end
