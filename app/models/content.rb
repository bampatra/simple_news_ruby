class Content < ActiveRecord::Base
    validates :news_title, presence: true,length: { in: 10..200 }
    VALID_URL_REGEX = /(^$)|(^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$)/ix
    validates :news_source, format: { with:  VALID_URL_REGEX }
    belongs_to :user
    has_many :comment
    
end
