require 'test_helper'

class CommentNewsControllerTest < ActionController::TestCase
  setup do
    @comment_news = comment_news(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:comment_news)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create comment_news" do
    assert_difference('CommentNew.count') do
      post :create, comment_news: { comment: @comment_news.comment, id: @comment_news.id }
    end

    assert_redirected_to comment_news_path(assigns(:comment_news))
  end

  test "should show comment_news" do
    get :show, id: @comment_news
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @comment_news
    assert_response :success
  end

  test "should update comment_news" do
    patch :update, id: @comment_news, comment_news: { comment: @comment_news.comment, id: @comment_news.id }
    assert_redirected_to comment_news_path(assigns(:comment_news))
  end

  test "should destroy comment_news" do
    assert_difference('CommentNew.count', -1) do
      delete :destroy, id: @comment_news
    end

    assert_redirected_to comment_news_path
  end
end
